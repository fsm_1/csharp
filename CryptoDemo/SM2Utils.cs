﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities.Encoders;
 

namespace CryptoDemo
{
    public class SM2Utils
    { 
        /// <summary>
        /// 生成SM2加密字符串
        /// </summary>
        /// <param name="pubKey"></param>
        /// <param name="srcData"></param>
        /// <returns></returns>
        public static byte[] EncryptPublic(String pubKey, byte[] srcData)
        {
            //byte[] publicKeyByte = Util4Hex.hexStringToBytes(pubKey);
            byte[] publicKeyByte = hexStringToBytes(pubKey);
            ECPublicKeyParameters ECPPublicKey = (ECPublicKeyParameters)PublicKeyFactory.CreateKey(publicKeyByte);
            SM2Engine engine = new SM2Engine();
            ParametersWithRandom pwr = new ParametersWithRandom(ECPPublicKey, new SecureRandom());
            engine.Init(true, pwr);
            return engine.ProcessBlock(srcData, 0, srcData.Length);
        }

        public static byte[] Decode(String privateKey, byte[] data)
        {
            ECPrivateKeyParameters PrivateKeyParameters = (ECPrivateKeyParameters) PrivateKeyFactory.CreateKey(hexStringToBytes(privateKey));
            var sm2 = new SM2Engine(new SM3Digest());
            sm2.Init(false, PrivateKeyParameters);
            return sm2.ProcessBlock(data, 0, data.Length);

        }

        /**
         * Convert hex string to byte[]
         * 
         * @param hexString the hex string
         * @return byte[]
         */
        public static byte[] hexStringToBytes(String hexString)
        {
            if (hexString == null || hexString.Equals(""))
            {
                return null;
            }
            hexString = hexString.ToUpper();
            int length = hexString.Length / 2;
            char[] hexChars = hexString.ToCharArray();
            byte[] d = new byte[length];
            for (int i = 0; i < length; i++)
            {
                int pos = i * 2;
                d[i] = (byte)(charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
            }
            return d;
        }

        /**
         * Convert char to byte
         * 
         * @param c char
         * @return byte
         */
        private static byte charToByte(char c)
        {
            return (byte)"0123456789ABCDEF".IndexOf(c);
        } 
    }
}
