﻿using Org.BouncyCastle.Utilities.Encoders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryptoDemo
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {


        public static string publicKey = "";
        public static string privateKey = "";
        public MainWindow()
        {
            InitializeComponent();
            SM2.GenerateKeyHex(out publicKey,out privateKey);
        }

        private void btnCrypto2_Click(object sender, RoutedEventArgs e)
        {
            if (txtContent.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入加密内容！");
                return;
            }
            string ec = txtContent.Text.Trim();
            string data = Base64.ToBase64String( Base64.Encode(Encoding.Default.GetBytes(ec)));          
           txtDecryptContent.Text = Encoding.Default.GetString( SM2Utils.EncryptPublic(publicKey, SM2Utils.hexStringToBytes(data)));

        }

        private void btnDecrypt2_Click(object sender, RoutedEventArgs e)
        {
            if(txtDecryptContent.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入解密内容！");
                return;
            }
           byte[] data = SM2Utils.Decode(privateKey, Encoding.Default.GetBytes(txtDecryptContent.Text.Trim()));

            MessageBox.Show("解密内容：" + Encoding.Default.GetString(Base64.Decode(data)));


        }
        private void btnCrypto3_Click(object sender, RoutedEventArgs e)
        {
            if (txtContent.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入加密内容！");
                return;
            }

        }

        private void btnDecrypt3_Click(object sender, RoutedEventArgs e)
        {
            if (txtDecryptContent.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入解密内容！");
                return;
            }

        }
        private void btnCrypto4_Click(object sender, RoutedEventArgs e)
        {
            if (txtContent.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入加密内容！");
                return;
            }
            string content = txtContent.Text.Trim();
            //content
            //Base64.Encode(  )
            SM4Utils tool = new SM4Utils();

            string dc = tool.Encrypt_ECB(content);
            txtDecryptContent.Text = dc;
        }

        private void btnDecrypt4_Click(object sender, RoutedEventArgs e)
        {
            if (txtDecryptContent.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入解密内容！");
                return;
            }
            SM4Utils tool = new SM4Utils();
            string dc = txtDecryptContent.Text.Trim();
            MessageBox.Show(tool.Decrypt_ECB(dc));
        }
    }
}
